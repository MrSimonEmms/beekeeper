# beekeeper

An application to enable beekeepers to keep track of their records and maintain
healthy apiaries.

# Getting Started

For best results, use [Docker Compose](https://docs.docker.com/compose/install).

In the root of your repo, run `docker-compose up`. This will run the development
stack and initialise test data sets.

# Contributing

See the [Contributing Guidlines](CONTRIBUTING.md).

# Applications

## WWW

- [Documentation](www)
- [Dev URL](http://localhost:9999)

The front-end application. This is written in TypeScript using the NuxtJS
application.

## Branches

### master

This is the mainline. Anything that goes in here is considered production-ready.
Any live bugfixes should originate from here.

Anything that gets pushed into here will use [Semantic Release](https://semantic-release.gitbook.io)
to generate a new version number. It is mandatory that you **MUST** use the
[git message convention](https://semantic-release.gitbook.io/semantic-release/#how-does-it-work).

### develop

The development mainline. This is considered a release candidate - that's to say
that anything in here should be considered ready for release and is pending
promotion to the `master` branch.

All pull requests must be made against this branch in the first instance. From
there, they will be pulled into the `master` branch as appropriate.

### feature/bugfix

Any development should be done in a branch that begins either `feature/` or
`bugfix/` and a pull request made.
