/**
 * logger
 *
 * Creates an instance of the logger to be
 * used server side.
 *
 * This should only be used outside of requests. In
 * most cases, use the req.log assigned to the
 * Request object
 */

/* Node modules */

/* Third-party modules */
import pino from 'pino';

/* Files */
import config from '../config';
import ServerLogger from './serverLogger';

export default new ServerLogger(pino({
  name: 'server',
  level: config.logger.level,
  serializers: pino.stdSerializers,
}));
