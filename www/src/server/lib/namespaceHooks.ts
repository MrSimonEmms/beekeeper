/**
 * namespaceHooks
 */

/* Node modules */

/* Third-party modules */
import cls from 'cls-hooked';
import { Request, Response, NextFunction } from 'express';
import uuid from 'uuid';

/* Files */

const nsp = uuid.v4();

export default {
  middleware: (_req: Request, _res: Response, next: NextFunction) => {
    const ns = cls.getNamespace(nsp) || cls.createNamespace(nsp);

    ns.run(() => next());
  },

  get<T>(key: string) : T | undefined {
    const ns = cls.getNamespace(nsp);

    if (ns && ns.active) {
      return <T> ns.get(key);
    }

    return undefined;
  },

  set<T>(key: string, value: T) : T | undefined {
    const ns = cls.getNamespace(nsp);

    if (ns && ns.active) {
      return ns.set<T>(key, value);
    }

    return undefined;
  },
};
