/**
 * serverLogger
 *
 * The logger to be used on the server. This uses cls-hooked
 * (which uses async_hooks) which is incompatible with web
 * work. This is to join the correlation IDs together
 * underneath
 */

/* Node modules */

/* Third-party modules */
import uuid from 'uuid';

/* Files */
import Logger from '../../lib/logger';
import namespaceHooks from './namespaceHooks';

export default class ServerLogger extends Logger {
  protected static logIdKey = 'logid';

  generateLogId(logId?: string) {
    if (logId) {
      /* Use existing log id */
      namespaceHooks.set<string>(ServerLogger.logIdKey, logId);
      this.trace('Correlation ID received');
    } else {
      namespaceHooks.set<string>(ServerLogger.logIdKey, uuid.v4());
      this.trace('Generating new correlation ID');
    }
  }

  protected getLogId(): string | undefined { // eslint-disable-line class-methods-use-this
    return namespaceHooks.get<string>(ServerLogger.logIdKey);
  }
}
