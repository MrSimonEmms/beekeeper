/**
 * app
 */

/* Node modules */

/* Third-party modules */
import { Module } from '@nuxt/types'; // eslint-disable-line import/no-unresolved
import express from 'express';

/* Files */
import logger from './lib/logger';
import namespaceHooks from './lib/namespaceHooks';
import routes from './routes';

declare global {
  namespace Express {
    export interface Request {
      correlationId: string;
      log: import('../interfaces/logger').ILogger;
    }
  }
}

/* Bootstrap express app */
async function main() : Promise<express.Express> {
  logger.debug('Creating express app');
  const app = express();

  /* Set up the middleware */
  app.set('x-powered-by', null);
  app.use(namespaceHooks.middleware);
  app.use((req: express.Request, _res: express.Response, next: express.NextFunction) => {
    /* Get the correlation ID from header, or generate our own */
    logger.generateLogId(req.header('x-correlation-id'));

    logger.debug('New HTTP request', {
      req,
    });

    next();
  });

  /* Build the routes */
  routes.forEach((route) => {
    app.use(route(express.Router()));
  });

  return app;
}

/* Register the app with nuxt */
const bootstrap : Module = function bootstrapper() {
  this.nuxt.hook('render:setupMiddleware', async () => {
    this.addServerMiddleware({
      path: '/api',
      handler: await main(),
    });
  });
};

export default bootstrap;
