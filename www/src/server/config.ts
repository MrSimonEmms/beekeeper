/**
 * config
 *
 * This is the server only config. For the front end
 * and nuxt config, see nuxt.config
 */

/* Node modules */

/* Third-party modules */

/* Files */

export default {
  logger: {
    level: process.env.LOG_LEVEL || 'info',
  },
};
