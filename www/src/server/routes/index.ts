/**
 * index
 */

/* Node modules */

/* Third-party modules */
import { Router } from 'express';

/* Files */

export interface IRoute {
  (route: Router): Router
}

/* Define the routes to run */
const routes : IRoute[] = [];

export default routes;
