/**
 * components
 */

/* Node modules */

/* Third-party modules */
import { Vue } from 'vue-property-decorator';

/* Files */
import BaseLayout from '../components/baseLayout.vue';

Vue.component('baseLayout', BaseLayout);
