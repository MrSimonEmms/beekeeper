/**
 * nuxt.config
 */

/* Node modules */

/* Third-party modules */

/* Files */

export default {
  srcDir: './src/',
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    titleTemplate(title: string) {
      const name = 'Beekeeper';
      if (title) {
        return `${name} | ${title}`;
      }
      return name;
    },
    meta: [{
      charset: 'utf-8',
    }, {
      name: 'viewport',
      content: 'width=device-width, initial-scale=1',
    }, {
      hid: 'description',
      name: 'description',
      content: process.env.npm_package_description || '',
    }],
    link: [{
      rel: 'icon',
      type: 'image/x-icon',
      href: '/favicon.ico',
    }],
  },
  /*
  ** Customize the progress-bar color
  */
  loading: {
    color: '#fff',
  },
  /*
  ** Global CSS
  */
  css: [],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    './plugins/components',
    './plugins/logger',
    './plugins/i18next',
    './plugins/vuelidate',
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    '@nuxt/typescript-build',
    '@nuxtjs/eslint-module',
    '@nuxtjs/vuetify',
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '~/server/app',
  ],
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {},
  /*
  ** vuetify module configuration
  ** https://github.com/nuxt-community/vuetify-module
  */
  vuetify: {
    customVariables: [
      '~/assets/variables.scss',
    ],
  },
  typescript: {
    typeCheck: {
      eslint: true,
    },
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
  },
  extends: [
    '@nuxtjs/eslint-config-typescript',
  ],
};
